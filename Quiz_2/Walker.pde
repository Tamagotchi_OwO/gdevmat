class Walker{
  float xPosition;
  float yPosition;
  float circleSize;
  float tx;
  float ty=10000;
  float ts;

Walker(){};
Walker (float x, float y, float s){
  xPosition=x;
  yPosition=y;
  circleSize=s;
}
float t = 0;
  void render(){
    
  noStroke();
  circle(xPosition*noise(tx), yPosition*noise(ty) ,circleSize*noise(ts));
  t+=0.01f;
  
}

void randomWalk(){
  
xPosition=map(noise(tx),0,1,Window.left,Window.right);
yPosition=map(noise(ty),0,1,Window.bottom,Window.top);
circleSize=map(noise(ts),0,1,10,250);

tx+=0.01f;
ty+=0.01f;
ts+=0.01f;
}
}
