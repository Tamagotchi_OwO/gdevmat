void setup()
{
size(1920,1080,P3D);
camera(0,0,-(height/2.0)/tan(PI*30/180.0),0,0,0,0,-1,0);
}
float tr= 100;
float tg= 150;
float tb= 200;
float mapped_red;
float mapped_green;
float mapped_blue;
PVector mousePos()
{
float x = mouseX - Window.windowWidth;
float y = -(mouseY - Window.windowHeight);
return new PVector(x,y);
}
void draw()
{
background (0);
fill (255,0,0);
//red
float r = noise(tr);
mapped_red=map(r,0,1,0,255);
//green
float g = noise(tg);
mapped_green = map(g,0,1,0,255);
//blue
float b = noise(tb);
mapped_blue = map(b,0,1,0,255);
tr+=0.01f;
tg+=0.01f;
tb+=0.01f;
PVector mouse=mousePos();
mouse.normalize();
mouse.mult(500);

//red saber
strokeWeight(15);
stroke(200,0,0);
line(mouse.x,mouse.y,-mouse.x,-mouse.y);

//saber glow
strokeWeight(10);
stroke(mapped_red,mapped_green,mapped_blue);
line(mouse.x,mouse.y,-mouse.x,-mouse.y);

//handle white center
strokeWeight(20);
stroke(255,255,255);
line(mouse.x/5,mouse.y/5,-(mouse.x/5),-(mouse.y/5));


}
