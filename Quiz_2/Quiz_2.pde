void setup()
{
size (1366,768,P3D);
camera(0,0,(height/2.0) / tan(PI*30.0/180.0),0,0,0,0,1,0);
background(0);
}
Walker walker = new Walker();

float tr = 100;
float tg = 150;
float tb = 200;
float mapped_red;
float mapped_green;
float mapped_blue;
void draw()
{

//color c = color(floor(random(255)),floor(random(255)),floor(random(255)));
//fill(c);

  float r = noise(tr);
  mapped_red = map(r, 0, 1, 0, 255);
  
  float g = noise(tg);
  mapped_green = map(g, 0, 1, 0, 255);
  
  float b = noise(tb);
  mapped_blue = map(b, 0, 1, 0, 255);
  
  tr += 0.01f;
  tg += 0.01f;
  tb += 0.01f;
  fill (mapped_red, mapped_green, mapped_blue);
  
  walker.randomWalk();
  walker.render();
}
