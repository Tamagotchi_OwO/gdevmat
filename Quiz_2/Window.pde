public static class Window
{
public static float heightPx = 768;
public static float widthPx = 1366;
public static float windowWidth = widthPx / 2;
public static float windowHeight = heightPx / 2;
public static float top = windowHeight;
public static float bottom= -windowHeight;
public static float left = -windowWidth;
public static float right = windowWidth;
  
}
